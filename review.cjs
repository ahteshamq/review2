const fs = require('fs')
const path = require('path')

const moviePath = path.join(__dirname, 'movies.json')

const movieData = fs.readFileSync(moviePath, 'utf-8')
const moviedata = JSON.parse(movieData)

function avgEarnGenre(movieData) {
    let earnData = movieData.reduce((accu, cur) => {
        if (cur.Gross_Earning_in_Mil !== 'NA') {
            if (accu.hasOwnProperty(cur.Genre)) {
                accu[cur.Genre]['count'] += 1
                accu[cur.Genre]['earn'] += parseFloat(cur.Gross_Earning_in_Mil) 
            } else {
                accu[cur.Genre] = {}
                accu[cur.Genre] = {
                    'count': 1,
                    'earn': parseFloat(cur.Gross_Earning_in_Mil)
                }
            }
        }
        return accu
    }, {})
    let res = Object.entries(earnData).map(movie => {
        let avg = movie[1].earn/movie[1].count
        return [movie[0], avg]
    })
    
    let resObj = res.reduce((accu,cur) => {
        if(accu.hasOwnProperty(cur[0])){

        }else{
            accu[cur[0]] = cur[1]
        }
        return accu
    },{})
    return resObj
}

console.log(avgEarnGenre(moviedata))


