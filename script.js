const ulCountry = document.querySelector('.country_list')
const loader = document.querySelector('.lds-ring')

fetch('https://restcountries.com/v3.1/all')
    .then(response => {
        if (!response.ok) {
            throw new Error('Could not fetch')
        } else {
            return response.json()
        }
    })
    .then(data => {
        loader.style.display = 'none'
        data.forEach(element => {
            createDiv(element, ulCountry)
        });
    })
    .catch(err => {
        console.log(err)
    })

function createDiv(element, parent) {

    const dataDiv = document.createElement('div')
    dataDiv.setAttribute('class', 'country')
    dataDiv.addEventListener('click', removeItem)

    const image = document.createElement('img')
    image.setAttribute('class', 'country-img')
    image.src = element.flags.png

    const name = document.createElement('h1')
    name.setAttribute('class', 'country-name')
    name.textContent = element.name.official

    dataDiv.appendChild(image)
    dataDiv.appendChild(name)

    ulCountry.appendChild(dataDiv)
}

function removeItem(event) {
    event.path[event.path.length - 6].style.display = 'none'
}